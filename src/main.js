import { homePage } from './home-page.js';
import { forecast } from './forecast.js';
import { weather } from './weather.js';
import { citiesGrid } from './cities-grid.js';
import { config } from './helpers/config.js';
import { database } from './helpers/database.js';

if (navigator.geolocation) {
  $(document).ready(database.getCurrentPosition());
}

$('#location').click(() => {
  forecast.renderCurrentLocation();
  weather.renderCurrentLocation();
  if (navigator.geolocation) {
    $('#daily').html(`<h5>Daily</h5>`);
    $('#hourly').html(`<h5>Next 24 hours</h5>`);
  }
});

$('#units-change').click(() => {
  const currentCity = database.unitsChange();
  forecast.render(currentCity);
  weather.render(currentCity);
  citiesGrid.renderFirst(config.firstPredefCity);
  citiesGrid.renderSecond(config.secondPredefCity);
  citiesGrid.renderThird(config.thirdPredefCity);
  citiesGrid.renderFourth(config.fourthPredefCity);
  citiesGrid.renderFifth(config.fifthPredefCity);
});

$('.navbar-brand').click(() => {
  homePage.render();
});

$('#search-city-button').click(() => {
  const searchString = $('#search-city-input').val();
  $('#daily').html(`<h5>Daily</h5>`);
  $('#hourly').html(`<h5>Next 24 hours</h5>`);
  forecast.render(searchString);
  weather.render(searchString);
});

$('#search-city-input').keypress((e) => {
  if (e.which === 13) {
    $('#search-city-button').click();
  }
});
$('#firstLocation')
    .on('click', () => {
      $('#daily').html(`<h5>Daily</h5>`);
      $('#hourly').html(`<h5>Next 24 hours</h5>`);
      forecast.render(config.firstPredefCity);
      weather.render(config.firstPredefCity);
    });

$('#secondLocation')
    .on('click', () => {
      $('#daily').html(`<h5>Daily</h5>`);
      $('#hourly').html(`<h5>Next 24 hours</h5>`);
      forecast.render(config.secondPredefCity);
      weather.render(config.secondPredefCity);
    });

$('#thirdLocation')
    .on('click', () => {
      $('#daily').html(`<h5>Daily</h5>`);
      $('#hourly').html(`<h5>Next 24 hours</h5>`);
      forecast.render(config.thirdPredefCity);
      weather.render(config.thirdPredefCity);
    });

$('#fourthLocation')
    .on('click', () => {
      $('#daily').html(`<h5>Daily</h5>`);
      $('#hourly').html(`<h5>Next 24 hours</h5>`);
      forecast.render(config.fourthPredefCity);
      weather.render(config.fourthPredefCity);
    });

$('#fifthLocation')
    .on('click', () => {
      $('#daily').html(`<h5>Daily</h5>`);
      $('#hourly').html(`<h5>Next 24 hours</h5>`);
      forecast.render(config.fifthPredefCity);
      weather.render(config.fifthPredefCity);
    });

homePage.render();

citiesGrid.renderFirst(config.firstPredefCity);
citiesGrid.renderSecond(config.secondPredefCity);
citiesGrid.renderThird(config.thirdPredefCity);
citiesGrid.renderFourth(config.fourthPredefCity);
citiesGrid.renderFifth(config.fifthPredefCity);
