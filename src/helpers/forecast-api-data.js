const getForecastData = (object) => {
  let firstMaxTemp = -Infinity;
  let firstMinTemp = Infinity;
  let firstDayOfTheWeekAbrev = '';

  let secondMaxTemp = -Infinity;
  let secondMinTemp = Infinity;
  let secondDayOfTheWeekAbrev = '';
  let secondWeatherMain;
  let secondWeatherIcon;

  let thirdMaxTemp = -Infinity;
  let thirdMinTemp = Infinity;
  let thirdDayOfTheWeekAbrev = '';
  let thirdWeatherMain;
  let thirdWeatherIcon;

  let fourthMaxTemp = -Infinity;
  let fourthMinTemp = Infinity;
  let fourthDayOfTheWeekAbrev = '';
  let fourthWeatherMain;
  let fourthWeatherIcon;

  let fifthMaxTemp = -Infinity;
  let fifthMinTemp = Infinity;
  let fifthDayOfTheWeekAbrev = '';
  let fifthWeatherMain;
  let fifthWeatherIcon;

  const UNIXdate = object.list[0].dt;
  const cityName = object.city.name;
  const countryCode = object.city.country;
  const correctTime = window.bundle.changeTimeZone(UNIXdate, cityName, countryCode);
  const startingDay = +(correctTime[8] + '' + correctTime[9]);
  const nextWeatherMain = object.list[0].weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const nextWeatherIcon = object.list[0].weather[0].icon;
  
  const daysArray = [];
  object.list.map((value) => {
    const currentUNIXdate = value.dt;
    const correctCurrentTime = window.bundle.changeTimeZone(currentUNIXdate, cityName, countryCode);
    const correctCurrentDay = +(correctCurrentTime[8] + '' + correctCurrentTime[9]);
    if (correctCurrentDay !== daysArray[daysArray.length - 1]) {
      daysArray.push(correctCurrentDay);
    }
  });

  object.list.map((value) => {
    const currentUNIXdate = value.dt;
    const correctCurrentTime = window.bundle.changeTimeZone(currentUNIXdate, cityName, countryCode);
    const correctCurrentDay = +(correctCurrentTime[8] + '' + correctCurrentTime[9]);
    
    if (correctCurrentDay === daysArray[0]) {
      firstMaxTemp = Math.round(Math.max(firstMaxTemp, value.main.temp));
      firstMinTemp = Math.round(Math.min(firstMinTemp, value.main.temp));
      
      firstDayOfTheWeekAbrev = correctCurrentTime[0] + correctCurrentTime[1] + correctCurrentTime[2];
    } else if (correctCurrentDay === daysArray[1]) {
      secondMaxTemp = Math.round(Math.max(secondMaxTemp, value.main.temp));
      secondMinTemp = Math.round(Math.min(secondMinTemp, value.main.temp));

      secondDayOfTheWeekAbrev = correctCurrentTime[0] + correctCurrentTime[1] + correctCurrentTime[2];
      const correctCurrentHour = correctCurrentTime[16] + '' + correctCurrentTime[17];

      if (correctCurrentHour >= 12 && correctCurrentHour < 15) {
        secondWeatherMain = value.weather[0].description.split(' ').map((word) => {
          return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
        secondWeatherIcon = value.weather[0].icon;
      }
    } else if (correctCurrentDay === daysArray[2]) {
      thirdMaxTemp = Math.round(Math.max(thirdMaxTemp, value.main.temp));
      thirdMinTemp = Math.round(Math.min(thirdMinTemp, value.main.temp));

      thirdDayOfTheWeekAbrev = correctCurrentTime[0] + correctCurrentTime[1] + correctCurrentTime[2];
      const correctCurrentHour = correctCurrentTime[16] + '' + correctCurrentTime[17];

      if (correctCurrentHour >= 12 && correctCurrentHour < 15) {
        thirdWeatherMain = value.weather[0].description.split(' ').map((word) => {
          return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
        thirdWeatherIcon = value.weather[0].icon;
      }
    } else if (correctCurrentDay === daysArray[3]) {
      fourthMaxTemp = Math.round(Math.max(fourthMaxTemp, value.main.temp));
      fourthMinTemp = Math.round(Math.min(fourthMinTemp, value.main.temp));

      fourthDayOfTheWeekAbrev = correctCurrentTime[0] + correctCurrentTime[1] + correctCurrentTime[2];
      const correctCurrentHour = correctCurrentTime[16] + '' + correctCurrentTime[17];

      if (correctCurrentHour >= 12 && correctCurrentHour < 15) {
        fourthWeatherMain = value.weather[0].description.split(' ').map((word) => {
          return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
        fourthWeatherIcon = value.weather[0].icon;
      }
    } else if (correctCurrentDay === daysArray[4]) {
      fifthMaxTemp = Math.round(Math.max(fifthMaxTemp, value.main.temp));
      fifthMinTemp = Math.round(Math.min(fifthMinTemp, value.main.temp));

      fifthDayOfTheWeekAbrev = correctCurrentTime[0] + correctCurrentTime[1] + correctCurrentTime[2];
      const correctCurrentHour = correctCurrentTime[16] + '' + correctCurrentTime[17];

      if (correctCurrentHour >= 12 && correctCurrentHour < 15) {
        fifthWeatherMain = value.weather[0].description.split(' ').map((word) => {
          return (word.charAt(0).toUpperCase() + word.slice(1));
        }).join(' ');
        fifthWeatherIcon = value.weather[0].icon;
      }
    }
  });
  return {
    firstMaxTemp,
    firstMinTemp,
    firstDayOfTheWeekAbrev,
    nextWeatherMain,
    nextWeatherIcon,
    secondMaxTemp,
    secondMinTemp,
    secondDayOfTheWeekAbrev,
    secondWeatherIcon,
    secondWeatherMain,
    thirdMaxTemp,
    thirdMinTemp,
    thirdDayOfTheWeekAbrev,
    thirdWeatherIcon,
    thirdWeatherMain,
    fourthMaxTemp,
    fourthMinTemp,
    fourthDayOfTheWeekAbrev,
    fourthWeatherIcon,
    fourthWeatherMain,
    fifthMaxTemp,
    fifthMinTemp,
    fifthDayOfTheWeekAbrev,
    fifthWeatherIcon,
    fifthWeatherMain,
  };
};

const getHourlyData = (data, hourIndex) => {
  const UNIXdate = data.list[hourIndex].dt;
  const cityName = data.city.name;
  const countryCode = data.city.country;
  const correctTime = window.bundle.changeTimeZone(UNIXdate, cityName, countryCode);
  const currentHour = `${correctTime[16]}${correctTime[17]}${correctTime[18]}${correctTime[19]}${correctTime[20]}`;
  const hourlyTemperature = Math.round(data.list[hourIndex].main.temp);
  const hourIcon = data.list[hourIndex].weather[0].icon;
  const hourWeatherDescription = data.list[hourIndex].weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');


  return {
    currentHour,
    hourlyTemperature,
    hourIcon,
    hourWeatherDescription,
  };
};

export const forecastData = {
  getForecastData,
  getHourlyData,
};
