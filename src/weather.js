import { database } from './helpers/database.js';

const convertTimestamp = (timestamp, cityName, countryCode) =>{
  const correctSunriseTime = window.bundle.changeTimeZone(timestamp, cityName, countryCode);
  const correctSunriseHour = `${correctSunriseTime[16]}${correctSunriseTime[17]}${correctSunriseTime[18]}${correctSunriseTime[19]}${correctSunriseTime[20]}`;
  return correctSunriseHour;
};

const getCurrentCityWeather = (data) => {
  const unitsSign = database.getCurrentUnitsSign();
  const windUnitsSign = database.getCurrentWindUnitsSign();
  const currentCityName = data.name;
  const currentCityCountryName = data.sys.country;
  const currentCityTemp = Math.round(data.main.temp);
  const currentCityWeatherType = data.weather[0].description.split(' ').map((word) => {
    return (word.charAt(0).toUpperCase() + word.slice(1));
  }).join(' ');
  const currentCityWeatherIcon = data.weather[0].icon;
  const humidity = data.main.humidity;
  const wind = data.wind.speed;
  const pressure = data.main.pressure;
  const sunrise = convertTimestamp(data.sys.sunrise, currentCityName, currentCityCountryName);
  const sunset = convertTimestamp(data.sys.sunset, currentCityName, currentCityCountryName);
  $('.current-weather').html(`
  <div class="container">
  <h1 class="text-center big-number">${currentCityName}, ${currentCityCountryName} <span id="geolocated"></span></h1>
  <div id="current-city" class="invisible hidden-text">${currentCityName}, ${currentCityCountryName}</div>
  <div class="row margin-top-left">
  <div class="col-2 no-padding text-center mb-3">
  <image src = ./src/images/icons/large/${currentCityWeatherIcon}.png></image>
  <div class="mb-3">
  <h3>${currentCityWeatherType}</h3>
  </div>
  </div>
  <div class="col-1 mr-4 no-padding pr-5">
  <h1 class="big-number">${currentCityTemp}${unitsSign}</h1>
  </div>
  <div class="mb-3 ml-5 pl-5">
  <h3><br>&emsp;&emsp;<image src = ./src/images/icons/second/humidity-50.png></image>&emsp;humidity: ${humidity}%
  <br>&emsp;&emsp;<image src = ./src/images/icons/second/wind-50.png></image>&emsp;wind: ${wind} ${windUnitsSign} &nbsp;
  <br>&emsp;&emsp;<image src = ./src/images/icons/second/pressure-52.png></image>&emsp;pressure: ${pressure} hpa &nbsp;<h3>
  </div>
  <div>
  <h3><br><image src = ./src/images/icons/second/sunrise-48.png></image>&emsp;sunrise: ${sunrise}
  <br><image src = ./src/images/icons/second/sunset-48.png></image>&emsp;sunset: ${sunset}</h3>
  </div>
  </div>
  </div>
  `).css('background-image', `url("./src/images/backgrounds/backgroundsCurrent/${currentCityWeatherIcon}.png")`).css('background-size', 'cover')
      .addClass(`forecast-border`);
  return data;
};

const getCurrentCityWeatherWithGeolocation = (data) => {
  getCurrentCityWeather(data);
  $('#geolocated').html('<image class="mb-4" src = ./src/images/location-48.png></image>');
};

const getDayOrNightBackground = (data) => {
  if (data.weather[0].icon[2] === 'd') {
    $('body').removeClass('bg-color-night').addClass('bg-color-day');
  } else if (data.weather[0].icon[2] === 'n') {
    $('body').removeClass('bg-color-day').addClass('bg-color-night');
  }
  return data;
};

const render = (cityName) => {
  database.getCurrentWeatherByName(cityName)
      .then(getDayOrNightBackground)
      .then(getCurrentCityWeather);
};

const renderCurrentLocation = () => {
  database.getCurrentPosition();
  database.getCurrentLocationWeather()
      .then(getDayOrNightBackground)
      .then(getCurrentCityWeatherWithGeolocation);
};

export const weather = {
  render,
  renderCurrentLocation,
};
